# before | after | total | client check | server check
reset
set encoding utf8
#set terminal postscript colour solid enhanced font "Arial, 20"
set terminal postscript solid enhanced font "Arial, 20"
set output '| ps2pdf - output_graph_mixed.pdf'
set style histogram rowstacked
set style data histograms
set style fill solid 1.0 border 0
#set xtics nomirror
#set ytics nomirror
set xlabel "message number"
#set ylabel "time in {/Symbol m}s"
set xrange [-1:275]
set yrange [-1:45000000]
set format y "%3.0s {/Symbol m}s"
plot "client_serverside.txt" using ($1-$3) title 'client-side processing' lc rgb "#1E90FF", \
     "" using 2 title 'client-side checks' lc rgb "#8B008B", \
     "" using 3 title 'server-side checks' lc rgb "#ADFF2F"
#, \
#     "" using ($3-$1-$2-$4-$5) title 'round-trip time' lc rgb "#DC143C"
