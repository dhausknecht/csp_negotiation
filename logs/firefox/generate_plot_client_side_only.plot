# total | client check
reset
set encoding utf8
set terminal postscript solid enhanced font "Arial, 20"
set output '| ps2pdf - output_graph_client_side_only.pdf'
set style histogram rowstacked
set style data histograms
set style fill solid 1.0 border 0
#set xtics nomirror
#set ytics nomirror
set xlabel "message number"
#set ylabel "time in {/Symbol m}s"
set xrange [-1:275]
#set yrange [-1:45000000]
set format y "%3.0s {/Symbol m}s"
# black and white:
#plot "combined_data_client_side_only.txt" using 2 title 'client-side check' lc rgb "#101010", \
#     "" using ($1-$2) title 'client-side processing' lc rgb "#A9A9A9"
# coloured:
# all whitelist_check server_check
plot "full_clientside.txt" using ($1-$2-$3) title 'client-side processing' lc rgb "#1E90FF", \
     "" using 2 title 'client-side check' lc rgb "#8B008B"
