# before | after | total | server checks
reset
set encoding utf8
set terminal postscript colour solid enhanced font "Arial, 20"
set output '| ps2pdf - output_graph_server_side_only.pdf'
set style histogram rowstacked
set style data histograms
set style fill solid 1.0 border 0
#set xtics nomirror
#set ytics nomirror
set xlabel "message number"
#set ylabel "time in {/Symbol m}s"
set xrange [-1:100]
set yrange [-1:45000000]
set format y "%3.0s {/Symbol m}s"
plot "combined_data_server_side_only.txt" using ($1+$2) title 'client-side processing' lc rgb "#1E90FF", \
     "" using 4 title 'server-side check' lc rgb "#ADFF2F", \
     "" using ($3-$1-$2-$4) title 'round-trip time' lc rgb "#DC143C"
