#!/bin/bash

cd extensions

echo "Looking for \".onHeadersReceived.addListener.*[.*\"responseHeaders\".*]\""
grep -zRil "\.onHeadersReceived\.addListener.*\[.*\"responseHeaders\".*\]" . > ../headersreceived_responseheaders.txt
wc -l ../headersreceived_responseheaders.txt

#echo "Looking for \"content-security-policy\""
#grep -Ril "\"content-security-policy\"" . > ../csp_files.txt
#wc -l ../csp_files.txt

#echo "Looking for both now"
#grep -zRil "\.onHeadersReceived\.addListener.*\[.*\"responseHeaders\".*\]" SEARCH_FOLDER | grep -i "\"content-security-policy\"" > ../headersreceived_responseheaders_csp.txt
# wc -l ../headersreceived_responseheaders_csp.txt
