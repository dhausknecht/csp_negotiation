#!/bin/bash

MAX_DIR_IN_ZIP=500
ARCHIVE_BASE_NAME="slice"
ARCHIVE_NR=0

zip_slices() {
  zip -q $1.zip $2
  echo "zipping $1: done"
}

ARCHIVE_NAME=$ARCHIVE_BASE_NAME"_"$ARCHIVE_NR
COUNTER=0
ZIP_ARG="-r "
for EXTENSION in ./*/; do
  ZIP_ARG=$ZIP_ARG" $EXTENSION"
  COUNTER=$((COUNTER+1))
  if [ "$COUNTER" -ge "$MAX_DIR_IN_ZIP" ]
  then
    COUNTER=0
    zip_slices $ARCHIVE_NAME "$ZIP_ARG" &
    ARCHIVE_NR=$((ARCHIVE_NR+1))
    ARCHIVE_NAME=$ARCHIVE_BASE_NAME"_"$ARCHIVE_NR
    ZIP_ARG="-r "
  fi
done
wait
echo "slicing done!"
