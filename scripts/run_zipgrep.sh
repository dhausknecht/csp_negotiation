#!/bin/bash

SEARCH_TERM="content-security-policy"

exec_zipgrep() {
  COUNT=$(zipgrep -i "$1" $2 | wc -l)
  echo "$COUNT occurrences of '$1' in $2"
  echo -n "$COUNT" > $BASHPID".tmp"
}

for EXTENSION in ./*.zip; do
  exec_zipgrep "$SEARCH_TERM" $EXTENSION &
  echo "$EXTENSION started"
done
wait
echo "all done"

SUM=0
for TMP_FILE in ./*.tmp; do
  SUM=$((SUM+$(cat "$TMP_FILE")))
  rm $TMP_FILE
done
echo "Total number of occurrences of '$SEARCH_TERM': $SUM"
