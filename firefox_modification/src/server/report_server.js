var https = require("https");
var http = require("http");
var fs = require('fs');

var options = {
  key: fs.readFileSync('/home/cookie/research/self_signed_keys/server.key'),
  cert: fs.readFileSync('/home/cookie/research/self_signed_keys/server.crt'),
  requestCert: false,
  rejectUnauthorized: false
};

main(process.argv);

function main(args) {
  //https.createServer(options, onRequest).listen(7777);
  http.createServer(onRequest).listen(7777);
  console.log("Report server has started.");
}

function onRequest(request, response) {
  console.log((new Date()).toString() + ":");
//  console.log(request.headers);
//  console.log();
  console.log("Method: " + request.method);
  console.log();
  if (request.method == "POST") {
    var body = "";
    request.on("data", function (data) {
      body += data;
    });
    request.on("end", function () {
      console.log("Report:\n" + body);
//      response.writeHead(200, {"Content-Type": "text/plain"});
//      response.write("Cheers!");
//      response.end();
      console.log();
      console.log("----------------------------");
    });
  } else {
    console.log("I no longer accept GET requests! Sorry...");
  } 
}
