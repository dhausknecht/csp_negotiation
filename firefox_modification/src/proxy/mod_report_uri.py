#!/usr/bin/python2.7

import imp
import json
import libmproxy.flow
import os
import re
from libmproxy.protocol.http import decoded


REPORT_URI = "http://localhost:7777 'allow-check'"
DEFAULT_SRC = "default-src 'none'"
#CONNECT_SRC = "connect-src *.google.com"

#host_filter = []
#whitelist = """{ "general":  ["http://example.com", "http://example2.com"
#                           , "https://rapportive.com", "https://*.linkedin.com"
#                           ],             
#               "report-uri": ["http://localhost:7777"]
#             }"""
#               "style-src": ["'unsafe-inline'", "'unsafe-eval'"],
#POLICY_FILE = "/home/cookie/research/csp_negotiation/code/firefox_modification/src/server/accept_rapportive.json"
POLICY_FILE = "/home/cookie/research/csp_negotiation/code/chrome_modification/src/server/partially_accept_rapportive.json"
#POLICY_FILE = "/home/cookie/research/csp_negotiation/code/chrome_modification/src/server/reject_rapportive.json"

f = open(POLICY_FILE, 'r');
whitelist = f.read();
whitelist = re.compile("\n").sub("", whitelist);
whitelist = re.compile("\s+").sub(" ", whitelist);

def response(context, flow):
    report_only_key = ""
    for key,csp in flow.response.headers:
        if re.match('content-security-policy(-report-only)?', key, re.IGNORECASE):
            # this is just a heuristic filter, nothing bullet-proof
            #host_filter.append(flow.request.get_host())
            #log("D - 1: ", str(csp))
            if re.match('content-security-policy-report-only', key, re.IGNORECASE):
                log("D", "Report only detected! " +  key)
                report_only_key = key
            if re.search('report-uri', csp, re.IGNORECASE):
                csp = re.sub(r"(?i)report-uri\s+.*;?", " report-uri "+REPORT_URI, csp)
            else:
                if not re.search(';$', csp, re.IGNORECASE):
                    csp = csp + ";"
                csp = csp + " report-uri "+REPORT_URI
            #log("D - 2: ", str(csp))
            #if not re.search('connect-src', csp, re.IGNORECASE):
            #    csp = CONNECT_SRC + '; ' + csp
            #if not re.search('default-src', csp, re.IGNORECASE):
            #    csp = DEFAULT_SRC + '; ' + csp
            flow.response.headers['Content-Security-Policy'] = [csp]
            flow.response.headers['csp-whitelist'] = [whitelist]
            #log("D", str(flow.response.headers['csp-whitelist']))
            break
    #log("D", "report_only_key = " + report_only_key)
    if len(report_only_key) > 0:
        del flow.response.headers[report_only_key]
        
''
def request(context, flow):
    host = flow.request.get_host()
    report = flow.request.content
    if flow.request.method == "POST" and (host in host_filter) and (len(report) > 0):
        # and flow.request.port == 8080:
        log("D", str(host) + "\n" + str(report) + "\n")
''

def log(mode, msg):
    if len(msg) > 1000:
      msg = msg[:994]+" [...]"
    print str(mode) + " [include_csp.py] - " + str(msg)
