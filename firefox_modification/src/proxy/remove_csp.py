#!/usr/bin/python2.7

import imp
import json
import libmproxy.flow
import os
import re
from libmproxy.protocol.http import decoded

CSP = ""

def response(context, flow):
    csp_keys = []
    effective_key = "Content-Security-Policy"
    for key,csp in flow.response.headers:
        if re.match('content-security-policy(-report-only)?', key, re.IGNORECASE):
            csp_keys.append(key)
    for key in csp_keys:
        del flow.response.headers[key]
    flow.response.headers[effective_key] = [CSP]
        

def log(mode, msg):
    if len(msg) > 100:
      msg = msg[:750]+" [...]"
    print str(mode) + " [include_csp.py] - " + str(msg)
