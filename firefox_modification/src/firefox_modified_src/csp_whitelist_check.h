#include <string>
#include <vector>
#include "json.h"

typedef std::map<std::string, std::vector<std::string>> CSP_map;

class CSPWhitelistCheck {
public:

  /*
   * Retreives the first report URI from the given CSP's report-uri directive
   * csp    - the CSP
   * return - the first found report URI
   */
  static std::string GetReportURI(const std::string csp);

  /*
   * Checks whether complement of original CSP and modified CSP is whitelisted, i.e. if 
   * "modified CSP" \ "original CSP" in whitelist.
   * old_csp       - the original CSP
   * new_csp       - the modified CSP
   * csp_whitelist - the whitelist in JSON format
   * return        - true if all domains in the complement are in the whitelist,
   *                 false otherwise.
   */
  static bool Check(std::vector<std::string>* old_csp,
                    std::vector<std::string>* new_csp,
                    std::string& csp_whitelist);

  /*
   * Tokenises a given string based on the delimiter.
   * str       - the string to tokenise
   * delimiter - the split criterion
   * return    - a vector with the string tokens as its elements
   */
  static std::vector<std::string> getTokens(std::string str, std::string delimiter);

  /*
   * Checks whether a given string is a value in a string-typed vector
   * str_vec - the string-typed vector
   * term    - the search term
   * return  - true if term is in str_vec, false otherwise
   */
  static bool inVector(std::vector<std::string>& str_vec, std::string term);

 private:
  static const std::string GET_DEFAULT_SRC() {
    return "default-src";
  };

  static const std::vector<std::string> GET_CSP_DIRECTIVES() {
    return {
    "base-uri",
    "child-src",
    "connect-src",
    "default-src",
    "font-src",
    "form-action",
    "frame-ancestors",
    "frame-src",
    "img-src",
    "media-src",
    "object-src",
    "plugin-types",
    "referrer",
    "reflected-xss",
    "report-uri",
    "sandbox",
    "script-src",
    "style-src"
    };
  };

  static CSP_map* mergeCSPs(std::vector<std::string>* const csp_strings);

  static CSP_map* parseCSP(std::string csp_string);

  static CSP_map* computeComplement(CSP_map* new_csp_map, CSP_map* old_csp_mapi);

  static CSP_map* createViolationReport(CSP_map* csp_complement_map, const Json::Value& whitelist_dict);

  static bool inDictionaryValue(const Json::Value& dict, std::string key, std::string term);

  CSPWhitelistCheck();
  virtual ~CSPWhitelistCheck();
};

// copied from http://www.cplusplus.com/faq/sequences/strings/trim/

inline std::string trim_right_copy(
  const std::string& s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return s.substr( 0, s.find_last_not_of( delimiters ) + 1 );
}

inline std::string trim_left_copy(
  const std::string& s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return s.substr( s.find_first_not_of( delimiters ) );
}

inline std::string trim_copy(
  const std::string& s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return trim_left_copy( trim_right_copy( s, delimiters ), delimiters );
}
