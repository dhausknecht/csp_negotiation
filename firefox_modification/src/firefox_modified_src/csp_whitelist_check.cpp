#include "csp_whitelist_check.h"

#include <map>
#include <algorithm>


/*
void printVector(std::vector<std::string>& v, std::string other_crap) {
  for (std::vector<std::string>::iterator i = v.begin(); i != v.end(); i++) {
    printf("???????[%s]: %s\n", other_crap.c_str(), i->c_str());
  }
}

void printCSPmap(CSP_map& csp_map, std::string name) {
  printf("%s\n", name.c_str());
  for(CSP_map::iterator it = csp_map.begin();
        it != csp_map.end(); it++) {
    printVector(it->second, name+"."+(it->first));
  }
}
//*/

std::string CSPWhitelistCheck::GetReportURI(const std::string csp) {
  CSP_map* csp_map = parseCSP(csp);
  for(CSP_map::iterator i = csp_map->begin(); i != csp_map->end(); i++) {
    std::string dir = i->first;
    std::transform(dir.begin(), dir.end(), dir.begin(), ::tolower);
    if (0 == dir.compare("report-uri")) {
      std::vector<std::string> dir_values = i->second;
      for (std::vector<std::string>::iterator i2 = dir_values.begin();
            i2 != dir_values.end(); i2++) {
        if (0 != i2->compare("'allow-check'")) {
          return *i2;
        }
      }
      return "";
    }
  }
  return "";
}

bool CSPWhitelistCheck::Check(std::vector<std::string>* const old_csp,
    std::vector<std::string>* const new_csp, std::string& csp_whitelist) {
  CSP_map* old_csp_map;
  CSP_map* new_csp_map;
  CSP_map* csp_complement_map;
  CSP_map* csp_violation_report;

  // Step 1: merge CSPs
  old_csp_map = mergeCSPs(old_csp);
  new_csp_map = mergeCSPs(new_csp);

  // Step 2: Compute complement
  csp_complement_map = computeComplement(new_csp_map, old_csp_map);

  // Step 3: Check for occurrences in whitelist
  Json::Value whitelist_dict;
  Json::Reader().parse(csp_whitelist, whitelist_dict);
  csp_violation_report = createViolationReport(csp_complement_map, whitelist_dict);

  bool result = (csp_violation_report->size() == 0);
  //printCSPmap(*csp_violation_report, "csp_violation_report:");

  return result;
}

CSP_map* CSPWhitelistCheck::mergeCSPs(std::vector<std::string>* const csp_strings) {
  CSP_map* merged_csp = NULL;
  for (std::vector<std::string>::iterator it = csp_strings->begin();
        it != csp_strings->end(); it++) {
    CSP_map* current_csp;
    current_csp = parseCSP(*it);
    if (it == csp_strings->begin()) {
      // it's the first one, so not much to do here...
      merged_csp = current_csp;
    } else {
      for(CSP_map::iterator it2 = current_csp->begin(); it2 != current_csp->end(); it2++) {
        std::string directive = it2->first;
        std::vector<std::string> dir_values = it2->second;
        if (merged_csp->count(directive) > 0) {
          // directive already exists -> intersect
          if (inVector((*merged_csp)[directive], "*")) {
            (*merged_csp)[directive] = dir_values;
          } else if (!inVector(dir_values, "*")) {
            std::vector<std::string> new_values;
            for (std::vector<std::string>::iterator it3 = (*merged_csp)[directive].begin();
                it3 != (*merged_csp)[directive].end(); it3++) {
              if (std::find(dir_values.begin(), dir_values.end(), *it3) != dir_values.end()) {
                new_values.push_back(*it3);
              }
            }
            (*merged_csp)[directive] = new_values;
          }
        } else {
          if (merged_csp->count(GET_DEFAULT_SRC()) > 0) {
            // fall back to default-src and merge
            if (inVector((*merged_csp)[GET_DEFAULT_SRC()], "*")) {
              if (!inVector(dir_values, "*")) {
                (*merged_csp)[directive] = dir_values;
              } // else fall back the next time again
            } else if (!inVector(dir_values, "*")) {
              bool at_least_one_diff = false;
              std::vector<std::string> new_values;
              for (std::vector<std::string>::iterator it3 = (*merged_csp)[GET_DEFAULT_SRC()].begin();
                  it3 != (*merged_csp)[GET_DEFAULT_SRC()].end(); it3++) {
                if (std::find(dir_values.begin(), dir_values.end(), *it3) != dir_values.end()) {
                  at_least_one_diff = true;
                  new_values.push_back(*it3);
                }
              }
              if (at_least_one_diff) {
                (*merged_csp)[directive] = new_values;
              }
            }
          } else {
            (*merged_csp)[directive] = dir_values;
          }
        }
      }
    }
  }
  return merged_csp;
}

CSP_map* CSPWhitelistCheck::parseCSP(std::string csp_string) {
  CSP_map* csp_map = new CSP_map();
  std::vector<std::string> tokens = getTokens(csp_string, ";");
  for (std::vector<std::string>::iterator it = tokens.begin();
        it != tokens.end(); it++) {
    std::vector<std::string> name_values = getTokens(*it, " ");
    std::string dir_name = *(name_values.begin());
    name_values.erase(name_values.begin());
    std::transform(dir_name.begin(), dir_name.end(), dir_name.begin(), ::tolower);
    if (!csp_map->count(dir_name)) {
      if (std::find(name_values.begin(), name_values.end(), "'none'")
          != name_values.end()) {
        (*csp_map)[dir_name] = std::vector<std::string>();
      } else {
        (*csp_map)[dir_name] = name_values;
      }
    }
  }
  return csp_map;
}

std::vector<std::string> CSPWhitelistCheck::getTokens(std::string str, std::string delimiter) {
  size_t pos = 0;
  std::vector<std::string> tokens;
  str = trim_copy(str);
  while ((pos = str.find(delimiter)) != std::string::npos) {
    std::string token = str.substr(0, pos);
    //std::cout << token << std::endl;
    token = trim_copy(token);
    if (token.length() > 0) {
      tokens.push_back(token);
    }
    //s.erase(0, pos + delimiter.length());
    str = str.substr(pos + delimiter.length());
  }
  if (str.length() > 0) {
    std::string token = str.substr(0, str.length());
    token = trim_copy(token);
    tokens.push_back(token);
  }
  return tokens;
}

CSP_map* CSPWhitelistCheck::computeComplement(CSP_map* new_csp_map, CSP_map* old_csp_map) {
  CSP_map* csp_complement_map = new CSP_map();
  std::vector<std::string> directives = GET_CSP_DIRECTIVES();
  for (std::vector<std::string>::iterator dir = directives.begin(); dir != directives.end(); dir++) {
    std::string directive = *dir;
    if ((new_csp_map->count(directive) > 0) || (old_csp_map->count(directive) > 0)) {
      std::vector<std::string> a;
      std::vector<std::string> b;
      CSP_map::iterator a_it = (new_csp_map->count(directive) > 0)
                                    ? new_csp_map->find(directive)
                                    : new_csp_map->find(GET_DEFAULT_SRC());
      if (a_it == new_csp_map->end()) { a.push_back("*"); } else { a = a_it->second; }
      CSP_map::iterator b_it = (old_csp_map->count(directive) > 0)
                                    ? old_csp_map->find(directive)
                                    : old_csp_map->find(GET_DEFAULT_SRC());
      if (b_it == old_csp_map->end()) { b.push_back("*"); } else { b = b_it->second; }

      bool star_in_a = inVector(a, "*");
      bool star_in_b = inVector(b, "*");

      std::vector<std::string> values;
      if (star_in_a && star_in_b) { continue; }
      if (!star_in_a && star_in_b) { continue; }
      if (star_in_a && !star_in_b) {
        values.push_back("*");
      } else {
        for (std::vector<std::string>::iterator it = a.begin(); it != a.end(); it++) {
          if (!inVector(b, *it)) {
            values.push_back(*it);
          }
        }
      }
      (*csp_complement_map)[directive] = values;
    }
  }
  return csp_complement_map;
}

bool CSPWhitelistCheck::inVector(std::vector<std::string>& str_vec, std::string term) {
  for (std::vector<std::string>::iterator it = str_vec.begin(); it != str_vec.end(); it++) {
    if (it->compare(term) == 0) { return true; }
  }
  return false;
}

CSP_map* CSPWhitelistCheck::createViolationReport(CSP_map* csp_complement_map, const Json::Value& whitelist_dict) {
  CSP_map* report = new CSP_map();
  for (CSP_map::iterator it = csp_complement_map->begin(); it != csp_complement_map->end(); it++) {
    std::string directive = it->first;
    std::vector<std::string> dir_values = it->second;
    std::vector<std::string> violations;
    if (inVector(dir_values, "*")) {
      violations.push_back("*");
    } else {
      for (std::vector<std::string>::iterator it2 = dir_values.begin();
          it2 != dir_values.end(); it2++) {
        if (!inDictionaryValue(whitelist_dict, "general", *it2)
            && !inDictionaryValue(whitelist_dict, directive, *it2)) {
          violations.push_back(*it2);
        }
      }
    }
    if (violations.size() > 0) {
      (*report)[directive] = violations;
    }
  }

  return report;
}

bool CSPWhitelistCheck::inDictionaryValue(const Json::Value& dict, std::string key, std::string term) {
  if (dict.isMember(key)) {
    const Json::Value& list_value = dict[key];
    if (list_value.isArray()) {
      for (int i = 0; list_value.isValidIndex(i); i++) {
        if (term.compare(list_value[i].asString()) == 0) { return true; }
      }
    }
  }
  return false;
}

CSPWhitelistCheck::CSPWhitelistCheck() { }
CSPWhitelistCheck::~CSPWhitelistCheck() { }

