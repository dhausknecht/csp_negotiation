#include "ObserversFinishedListener.h"

#include "nsIHttpProtocolHandler.h"
#include "mozilla/net/HttpBaseChannel.h"
#include "csp_whitelist_check.h"
#include "nsIIOService.h"

// for performance tests:
#include <chrono>
#include <iostream>
#include <fstream>

#define CSP_HEADER_STR "content-security-policy"

ObserversFinishedListener::ObserversFinishedListener() { };
ObserversFinishedListener::~ObserversFinishedListener() { };

void ObserversFinishedListener::Notify(nsISupports* aSubject, const char* aTopic) {
  std::chrono::high_resolution_clock::time_point t_notify_start = std::chrono::high_resolution_clock::now();

  // no examination of responses? This is not the droid we are looking for...
  if (0 != strcmp(aTopic, NS_HTTP_ON_EXAMINE_RESPONSE_TOPIC)) { return; }
  //rintf("Infiltration complete!\n");

  void* baseChan_void;
  aSubject->QueryInterface(NS_HTTPCHANNEL_IID, &baseChan_void);
  mozilla::net::HttpBaseChannel* baseChan = (mozilla::net::HttpBaseChannel*) baseChan_void;
  baseChan_void = nullptr;

  // no modification? This is not the droid we are looking for...
  if (!baseChan->GetCSPHeaderModified()){ return; }

  nsCString original_header_value;
  baseChan->GetOriginalCSPHeader(original_header_value);
  //rintf("original CSP: %s\n", original_header_value.get());

  // no support for negotiation? This is not the droid we are looking for...
  if (!original_header_value.IsEmpty()
      && !SupportsCSPNegotiation(original_header_value)) { return; }

  nsCString new_header_value;
  baseChan->GetResponseHeader(NS_LITERAL_CSTRING(CSP_HEADER_STR), new_header_value);
  //rintf("%s: %s %i \n", CSP_HEADER_STR, new_header_value.get(), new_header_value.IsEmpty());

  // OPTIONAL: check if there is a diff to original
  // no: return to normal
  // XXX not implemented yet, maybe never... :-O

  // but THIS is the droid we are looking for... :-D
  // check if whitelist exists:
  std::chrono::high_resolution_clock::time_point t_clientside_start = std::chrono::high_resolution_clock::now();
  nsCString whitelist;
  baseChan->GetNegotiationWhitelist(whitelist);
  bool whitelist_accept = CheckWhitelist(original_header_value, new_header_value, whitelist);
  //rintf("csp-whitelist: %s\n", whitelist.get());
  std::chrono::high_resolution_clock::time_point t_clientside_end = std::chrono::high_resolution_clock::now();
  // accept: return to normal
  if (whitelist_accept) {
    //rintf("CSP modification was accepted!\n");
    std::ofstream logfile;
    logfile.open ("/home/cookie/research/csp_negotiation/code/logs/firefox/overheads_combined.txt", std::ios::out | std::ios::app );
    logfile << std::chrono::duration_cast<std::chrono::nanoseconds>(t_clientside_end-t_notify_start).count() << "    " // t_notify_end == t_clientside_end
            << std::chrono::duration_cast<std::chrono::nanoseconds>(t_clientside_end-t_clientside_start).count() << "    "
            << 0 << '\n';
    logfile.close();
    return;
  }
  //rintf("csp-whitelist: mod rejected by whitelist - IsEmpty%i \n", whitelist.IsEmpty());

  // reject: compute message to server
  std::chrono::high_resolution_clock::time_point t_serverside_start = std::chrono::high_resolution_clock::now();
  nsCString csp_report;
  nsCString host; baseChan->GetRequestHeader(nsCString("Host"), host);
  GenerateNegotiationMessage(new_header_value, host, original_header_value, csp_report);
  
  // send negotiation message and wait for reply
  std::string url_string = CSPWhitelistCheck::GetReportURI(std::string(original_header_value.get()));
  nsCString url(url_string.c_str());
  nsCString serverReply;
  SendNegotiationMessage(csp_report, url, serverReply);
  std::chrono::high_resolution_clock::time_point t_serverside_end = std::chrono::high_resolution_clock::now();

  // enforce decision consequences
  if (!serverReply.EqualsIgnoreCase("Accept")) {
    baseChan->SetResponseHeader(NS_LITERAL_CSTRING(CSP_HEADER_STR), original_header_value, false);
  }
  std::chrono::high_resolution_clock::time_point t_notify_end = std::chrono::high_resolution_clock::now();

  // store times to logfiles
  std::ofstream logfile;
  logfile.open ("/home/cookie/research/csp_negotiation/code/logs/firefox/overheads_combined.txt", std::ios::out | std::ios::app );
  logfile << std::chrono::duration_cast<std::chrono::nanoseconds>(t_notify_end-t_notify_start).count() << "    "
          << std::chrono::duration_cast<std::chrono::nanoseconds>(t_clientside_end-t_clientside_start).count() << "    "
          << std::chrono::duration_cast<std::chrono::nanoseconds>(t_serverside_end-t_serverside_start).count() << '\n';
  logfile.close();
}

bool ObserversFinishedListener::SupportsCSPNegotiation(const nsCString& csp_header) {
  std::string csp_string(csp_header.get());
  bool allow_check = false;
  std::vector<std::string> report_uri_tokens;
  std::string report_uri_string = "report-uri";
  std::string allow_check_string = "'allow-check'";
  // TODO: this assumes a single header for now. The standard allows multiple though!
  std::vector<std::string> name_values = CSPWhitelistCheck::getTokens(csp_string, ";");
  for (std::vector<std::string>::iterator i = name_values.begin();
        !allow_check && i != name_values.end(); i++) {
    report_uri_tokens = CSPWhitelistCheck::getTokens(*i, " ");
    allow_check = CSPWhitelistCheck::inVector(report_uri_tokens, report_uri_string)
                    && CSPWhitelistCheck::inVector(report_uri_tokens, allow_check_string);
  }
  return allow_check;
}

bool ObserversFinishedListener::CheckWhitelist(const nsACString& old_csp, 
        const nsACString& new_csp, const nsACString& whitelist) {
  if (!whitelist.IsEmpty()) {
    // yes: check compliance of change with whitelist
    std::vector<std::string> old_csp_vec;
    std::vector<std::string> new_csp_vec;
    std::string whitelist_string(ToNewCString(whitelist));
    old_csp_vec.push_back(std::string(ToNewCString(old_csp)));
    new_csp_vec.push_back(std::string(ToNewCString(new_csp)));

    return CSPWhitelistCheck::Check(&old_csp_vec, &new_csp_vec, whitelist_string);
  }
  return false;
}

void ObserversFinishedListener::GenerateNegotiationMessage(const nsACString& new_csp, 
        const nsACString& host, const nsACString& old_csp, nsACString& msg) {
  msg.Append("{ \"content-security-policy\": [\"");
  msg.Append(new_csp); // we only assume a single CSP now
  msg.Append("\"], \"original_url\": \"");
  msg.Append(host);
  msg.Append("\", \"original_csp\": [\""); // we only assume a single CSP now
  msg.Append(old_csp);
  msg.Append("\"] }");
}

void ObserversFinishedListener::SendNegotiationMessage(const nsACString& msg, 
        const nsACString& server_url, nsACString& serverReply) {
  nsresult rv;
  nsCOMPtr<nsIIOService> ioService(do_GetService(NS_IOSERVICE_CONTRACTID, &rv));
  nsIChannel* neg_server_chan;
  ioService->NewChannel(server_url, nullptr, nullptr, &neg_server_chan);

  // code pretty much just copied from dom/security/nsCSPContext.cpp
  nsCOMPtr<nsIStringInputStream> sis(do_CreateInstance(NS_STRINGINPUTSTREAM_CONTRACTID));
  sis->SetData(ToNewCString(msg), msg.Length());

  nsCOMPtr<nsIUploadChannel> uploadChannel(do_QueryInterface(neg_server_chan));
  uploadChannel->SetUploadStream(sis, NS_LITERAL_CSTRING("application/json"), -1);

  nsCOMPtr<nsIHttpChannel> httpChannel(do_QueryInterface(neg_server_chan));
  if (httpChannel) {  httpChannel->SetRequestMethod(NS_LITERAL_CSTRING("POST")); }

  //rintf("new channel to %s established...\n", ToNewCString(server_url));
  nsIInputStream* neg_response_stream;
  neg_server_chan->Open(&neg_response_stream);
  //rintf("channel opened...\n");
  uint32_t read_count = 0;
  do {
    uint32_t aCount = 32;
    char* inBuf = (char*)malloc(aCount); inBuf[aCount-1] = '\0';
    neg_response_stream->Read(inBuf, aCount-1, &read_count);
    inBuf[read_count] = '\0';
    serverReply.Append(nsCString(inBuf));
  } while (read_count > 0);
  //rintf("Server decision: %s\n", ToNewCString(serverReply));
  neg_response_stream->Close();
  //rintf("Input stream closed!\n");
}
