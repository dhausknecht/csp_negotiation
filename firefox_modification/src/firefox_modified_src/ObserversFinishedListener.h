#ifndef ObserverFinishedListener_h__
#define ObserverFinishedListener_h__

#include "nsISupports.h"

class ObserversFinishedListener {

public:
  ObserversFinishedListener();
  virtual ~ObserversFinishedListener();

  void Notify(nsISupports* aSubject, const char* aTopic);

private:
  bool SupportsCSPNegotiation(const nsCString& csp_header);

  bool CheckWhitelist(const nsACString& old_csp, 
                      const nsACString& new_csp,
                      const nsACString& whitelist);

  void GenerateNegotiationMessage(const nsACString& new_csp,
                                  const nsACString& host,
                                  const nsACString& old_csp,
                                  nsACString& msg);

  void SendNegotiationMessage(const nsACString& msg,
                              const nsACString& server_url,
                              nsACString& serverReply);
};
#endif /* ObserverFinishedListener_h__ */
