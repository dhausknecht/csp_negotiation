+-----------------------------------------------------------------------------+
| Table of Contents:                                                          |
+-----------------------------------------------------------------------------+
1. List of modified files
2. List of added files
3. Notes (incl. "General setup")
4. Considerations (i.e. conceptual questions)
5. TODOs          (i.e. implementation issues)


+-----------------------------------------------------------------------------+
| 1. List of modified files:                                                  |
+-----------------------------------------------------------------------------+
+ ./extensions/extensions.gyp
+ ./chrome/browser/extensions/api/web_request/web_request_api.cc
+ ./chrome/browser/extensions/api/web_request/web_request_api.h

1.1 "chrome_browser_extensions.gypi":
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
It is basically a list of files to include in different build configurations.
Therefore, search for the string (for respecting the alphabetic order):
    'browser/api/web_request/form_data_parser.cc',
and above that one, add the following lines:
    'browser/api/web_request/csp_modification_delegate.cc',
    'browser/api/web_request/csp_modification_delegate.h',
    'browser/api/web_request/csp_whitelist_check.h',
    'browser/api/web_request/csp_whitelist_check.cc',
    'browser/api/web_request/event_handled_data.h',
    'browser/api/web_request/event_handled_data.cc',

1.2 "web_request_api.cc" and "web_request_api.h":
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The basic change is that in ExtensionWebRequestEventRouter::OnEventHandled,
the function called after a event return, it is checked if we return from 
a OnHeadersReceived event. If yes and there are changes in the CSP, we create 
a request to a CSP modification check server (read the paragraph "CSP check 
server" in Notes for more information). We do not proceed handling the event 
in the browser until we get a reply from the server. The request event 
callbacks are all handled the CPModificationDelegate implementation (see list 
of added files). On the server reply the ExtensionWebRequestEventRouter takes 
over again: first, the server reply is handled (respecting the "Accept" or 
"Reject" decision) and then continues as in the original implementation with 
DecrementBlockCount().

1.3 Generally about other source files:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A simple insert/replace should do the trick here... (not tested with an 
un-modified version though. Important: read the "Repo changes" note).


+-----------------------------------------------------------------------------+
| 2. List of added files:                                                     |
+-----------------------------------------------------------------------------+
+ ./chrome/browser/extensions/api/web_request/csp_modified_delegate.h
+ ./chrome/browser/extensions/api/web_request/csp_modified_delegate.cc
+ ./chrome/browser/extensions/api/web_request/event_handled_data.h
+ ./chrome/browser/extensions/api/web_request/event_handled_data.cc
+ ./chrome/browser/extensions/api/web_request/csp_whitelist_check.h
+ ./chrome/browser/extensions/api/web_request/csp_whitelist_check.cc


+-----------------------------------------------------------------------------+
| 3. Notes:                                                                   |
+-----------------------------------------------------------------------------+
3.1 General setup:
~~~~~~~~~~~~~~~~~~
Run the provided 'run_code.sh' bash script, or go through the following steps:

Step 1) Start 'mitmdump' in order to change the CSP from report-only to 
enforce mode and to modify the report-uri to 'http://localhost:7777':
  mitmdump -q -s mod_report_uri.py

Step 2) Start the report server with:
  nodejs report_server.js

Step 3) Start the CSP check server with:
  nodejs csp_server.js ["Accept"|"Reject"|--policy=path/to/file]
Using the first two options allows to set a "always accept/reject, no matter 
what". The last option reads a policy in JSON format from file. Read Section 
3.5 about the whitelist definition and enforcement rules.

Step 4) Compile the modified browser and start it with the following flags:
  path/to/chrome --proxy-server=localhost:8080 --allow-running-insecure-content

Step 5) Visit what ever service you want to check, e.g. mail.google.com with 
rapportive installed.


3.2 Repo changes:
~~~~~~~~~~~~~~~~~
The code is now upated to work with Chrome version 41 (as of 28. Nov 2014).


3.3 CSP negotiation server:
~~~~~~~~~~~~~~~~~~~~~~~~~~~
The 'report-uri' directive is used as the "report" address: in report-only 
mode, no reply is expected (literally report-only). However in enforcement 
mode, the user agent may send a check request. 'allow-checks' indicates 
that the server is ready to accept CSP negotiation requests. This allows 
compatibility with current standards that do not include this feature.


3.4 Sending of new CSP:
~~~~~~~~~~~~~~~~~~~~~~~
The CSP is sent in JSON format to the CSP check server. The JSON string is 
sent as the body of a POST request. The format is as follows:
  { "Content-Security-Policy": [list of all CSP headers],
    "original_url": <the request url for the modified response> }
Note, that we send a list of CSPs since the standard allows for multiple 
definitions. The decision to use JSON is two-fold:
1) it is well-known, widespread and easy to process
2) it is also used for the 'report-url' directive which makes it more 
   compatible with the standard


3.5 Whitelist policy definition and enforcement:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Expected JSON format:
 - the property values must be a list of URLs/key words
 - "general" defines generally accepted URLs/key words independet of 
   direcitve. This porperty is optional.
 - any other property must be named after a CSP directive, specifying the 
   whitelisted URLs/key words for this directive only. This is options, i.e. 
   a directive must not appear in the JSON object.

A URL/key word is whitelisted for a directive if it is included in the 
"general" property and/or the respective directive porperty.


3.6 Whitelists and client-side decision making:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The CSP check request must be set before rendering the page, i.e. the checking 
is blocking. Thus, delays through the extra round-trip double the page loading 
time. Therefore, additionally to the actual CSP, the requested server can send 
a (part of the) whitelist with, e.g. the most common, accepted domains. This 
allows pre-processing and decision making on the client-side. If all 
additional URLs in the modified CSP appear in the whitelist, the user agent 
can skip sending the check request and proceed as when receiving a positiv 
decision ("Accept") from the server. In case of a negative client-side 
decision, the user agent has to send the check request as if no whitelist was 
present.

Candidates for the whitelist can be identified during a report-only phase 
but also during an actual checking (simple most-occurences statistics).

Besides the behaviour described in this section, the format and other 
semantics of the sent whitelist is the same as described in Section 3.4 and 
Section 3.5.


+-----------------------------------------------------------------------------+
| 4. Considerations:                                                          |
+-----------------------------------------------------------------------------+
This section lists general considerations and issues not yet solved. Concret 
implementation issues are meant to be listed in Section 5 "TODOs".

4.1 CSP check server definition:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
As an alternative approach to the one in Section 3.3 for CSP negotiation 
server URL definition we could also introduce a new directive 'check-uri'.


4.2 CSP vs. whitelist:
~~~~~~~~~~~~~~~~~~~~~~
Main motiviation to have a separate whitelist, i.e. the accepted 
additional URLs are not part of the actual CSP:
"as restrictive as possible": in order to get the most out of the CSP 
mechanism, the delivered CSP should be as restrictive as possible to reduce 
the attack surface for XSS. Therefore, immediately adding the whitelisted 
URLs unnecessarily opens up a web page for those domains.

Alternative apporach: CSP modification list
Extensions notify the browser which about modifications they want to perform,
e.g. "add 'unsafe-inline' to script-src". The browser sends this information 
with the initial request to the server. The server considers this information 
when creating and sending the CSP with the response. Extensions can then not 
modify the response header's CSP value.
Advantage: - only one round-trip for negotiating the CSP between extensions 
             and server. In particular, no blocking check request.
           - backwards compatible with current standards.
Drawback: - extensions must know which modifications they want to perform 
            which becomes problematic, e.g., for re-directs, i.e. when the 
            rendered web page differs from the originally requested one (more 
            precisely, its domain).
          - a mechanism is required that can determine the list of desired  
            additional values. This can be done by either the extension 
            developer statically (fixed list) or dynamically (function that 
            returns values based on request information), by the extension 
            store (code analysis), or the browser (code analysis). Though 
            for the options respectively more or less severely, all of them 
            struggle when it comes to dynamic loading of code (and its 
            behaviour prediction) for the extension itself.


4.3 Network security:
~~~~~~~~~~~~~~~~~~~~~
Should a secured connection be enforced for transmitting the CSP check request 
(cf. mixed content blocking, i.e. HTTP content and HTTPS content)?
First thoughts:
If 4.1.2, this would be for free in Chrome since it doesn't allow HTTP 
report-uris for HTTPS content.
See also the specification for Mixed Content by W3C under:
    http://www.w3.org/TR/mixed-content/


+-----------------------------------------------------------------------------+
| 5. TODOs:                                                                   |
+-----------------------------------------------------------------------------+
This section provides a list of more general TODOs for implementation. Be 
aware that this list is under constant change. The numbering should be 
incremental only to avoid collisions/confusions with previous TODOs.
