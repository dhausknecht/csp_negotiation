Chromium  41.0.2233.0 (Developer Build) 
Revision  793ee5bff54aa3cb1c1873dfeeeb1f36c42e810f-refs/heads/master@{#306027}
OS  Linux 
Blink 537.36 (@186136)
JavaScript  V8 3.31.25
Flash (Disabled)
User Agent  Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2233.0 Safari/537.36
Command Line  out/Debug/chrome --flag-switches-begin --flag-switches-end
Executable Path /home/cookie/chromium/src/out/Debug/chrome
Profile Path  /home/cookie/.config/chromium/Default
Variations  InfiniteCache:No
            Prerender:PrerenderEnabled
            UMA‑New‑Install‑Uniformity‑Trial:Experiment
            UMA‑Session‑Randomized‑Uniformity‑Trial‑5‑Percent:group_04
            UMA‑Uniformity‑Trial‑1‑Percent:group_87
            UMA‑Uniformity‑Trial‑10‑Percent:default
            UMA‑Uniformity‑Trial‑100‑Percent:group_01
            UMA‑Uniformity‑Trial‑20‑Percent:default
            UMA‑Uniformity‑Trial‑5‑Percent:group_10
            UMA‑Uniformity‑Trial‑50‑Percent:group_01
