// --------------------- includes -----------------------
var http = require("http");
//var url = require("url");
//var qs = require("querystring");
var fs = require("fs");

// ----------------- global variables -------------------
var PORT = 7777;
var STATIC_SERVER_DECISION = null;

// Note, we don't consider "report-uri" here. Maybe we should...?
var CSP_DIRECTIVES = [
    "base-uri",
    "child-src",
    "connect-src",
    "default-src",
    "font-src",
    "form-action",
    "frame-ancestors",
    "frame-src",
    "img-src",
    "media-src",
    "object-src",
    "plugin-types",
    "referrer",
    "reflected-xss",
    "report-uri",
    "sandbox",
    "script-src",
    "style-src"
    ];

var whitelist = [];


// make it all run!
main(process.argv);

// --------------- function definitions -----------------

function main(args) {
  var file_path = null;

  for (var i = 2; i < args.length; i++) {
    console.log("\t" + args[i]);
    if (args[i] == "Reject" || args[i] == "Accept") {
      STATIC_SERVER_DECISION = sDecision;
    } else 
    if (args[i].match(/--policy=.*/i)) {
      file_path = args[i].split("=")[1];
    }
  }

  if (STATIC_SERVER_DECISION == null && file_path == null) {
    console.log("Usage: nodejs " + args[1] + " [\"Accept\"|\"Reject\"|--policy=path/to/file]");
  } else {
    readWhitelist(file_path, http.createServer(onRequest).listen(PORT));
    console.log("Server has started.");
  }
}

function onRequest(request, response) {
  request.processing_time = process.hrtime();

  console.log((new Date()).toString() + ":");
  console.log(request.headers);
  console.log("Method: " + request.method);
  if (request.method == "POST") {
    var body = "";
    request.on("data", function (data) {
      body += data;
    });
    request.on("end", function () {
      body = JSON.parse(body);
      if (isCheckRequest(body)) {
        var decision = makeServerDecision(body);
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write(decision);
        response.end();
        console.log("Server decision: " + decision);
        console.log("----------------------------");
        console.log();
      } else if (body["csp-report"]["blocked-uri"]) {
        console.log("CSP report:");
        console.log("-----------");
        console.log("\tblocked-uri: " + body["csp-report"]["blocked-uri"]);
        console.log("\tviolated-directive: " + body["csp-report"]["violated-directive"]);
        console.log();
      } else {
        console.log("Whatever it was, I just ignore it...");
        console.log();
        console.log(body);
        console.log();
      }
      var delta_t = process.hrtime(request.processing_time); // [secs, nano-secs]
      delta_t = (delta_t[0]*1000000000)+delta_t[1];
      fs.appendFile('/home/cookie/research/csp_negotiation/code/logs/server_side_check_time_logs.txt', delta_t+'\n', function (err) {
        if (err != null) {
          console.log("Failed to log time diff: " + err);
        }
      });
    });
  } else {
    console.log("I no longer accept GET requests! Sorry...");
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("Reject");
    response.end();
  } 
}

function isCheckRequest(check_request) {
  return check_request["content-security-policy"]
          && check_request["original_url"]
          && check_request["original_csp"];
}

// Makes the actual accept/reject decision.
// check_request - the JSON formatted string sent by the user agent
// return        - allways either "Accept" or "Reject"
function makeServerDecision(check_request) {
  if (STATIC_SERVER_DECISION != null) {
    return STATIC_SERVER_DECISION;
  }
  var mod_csps = check_request["content-security-policy"].map(function(policy) {
    return parseCSP(policy);
  });
  var original_csps = check_request["original_csp"].map(function(policy) {
    return parseCSP(policy);
  });
  var merged_mod_csp = mergeCSPs(mod_csps);
  var merged_original_csp = mergeCSPs(original_csps);
  console.log();
  /*
  console.log("Merged CSP: " + JSON.stringify(merged_mod_csp));
  console.log("Merged CSP: " + JSON.stringify(merged_original_csp));
  console.log();
  */

  var violation_report = matchAgainstWhitelist(merged_original_csp, merged_mod_csp, whitelist);
  if (violation_report == null) {
    return "Accept";
  } else {
    console.log("The modified CSP was rejected because of the following sources in the respective directives:");
    console.log(JSON.stringify(violation_report));
    return "Reject";
  }
}

// Parse the CSP according to "4.1.1 Parsing Policies" in the standard
// return - an object with the directives as properties. The directive 
//          value is a list of directive-value strings
function parseCSP(csp_string) {
  var directives = {}; // step 1
  csp_string.split(";").forEach(function(token) { // step 2
    token = token.trim(); // ste 2.1
    name_values = token.split(" ");
    directive_name = name_values.shift().toLowerCase(); // step 2.2, 2.3, 2.4
    if (!(directive_name in directives)) { // step 2.5
      // parse source list according to "4.2.1 Parsing Source Lists" in 
      // standard. We omit the source expression matching here though.
      var none = false;
      name_values = name_values.map(function(value) {
        var result = value.trim();
        none = none || (result == "'none'");
        return result;
      });
      if (none) { name_values = []; }
      directives[directive_name] = name_values; // step 2.6
    }
  });
  //console.log("Paresd CSP: " + JSON.stringify(directives));
  return directives;
}

// merge a list of CSPs to a single one that is effecively enforced
// return - the effectively enforced CSP as an object
function mergeCSPs(csp_list) {
  // general note: 'none'==empty set, but '*' must be handled extraly
  if (csp_list.length <= 0) { return {}; }
  var DEFAULT_SRC = "default-src";
  var merged_directives = csp_list.shift();

  csp_list.forEach(function(csp) {
    Object.keys(csp).forEach(function(directive) {
      if (directive in merged_directives) {
        // directive already exists -> intersect
        if (merged_directives[directive].indexOf("*") >= 0) {
          merged_directives[directive] = csp[directive];
        } else if (csp[directive].indexOf("*") < 0) {
          var new_values = merged_directives[directive].filter(function(value) {
            return (csp[directive].indexOf(value) != -1);
          });
          merged_directives[directive] = new_values;
        }
        // else csp[directive] is "*" and 
        // merged_directives[directive] is not "*" but exists
        // -> it must be more restrictive than csp[directive] already
      } else {
        if (DEFAULT_SRC in merged_directives) {
          // fall back to default-src and merge
          if (merged_directives[DEFAULT_SRC].indexOf("*") >= 0) {
            if (csp[directive].indexOf("*") < 0) {
              merged_directives[directive] = csp[directive];
            } // else fall back the next time again
          } else if (csp[directive].indexOf("*") < 0) {
            var at_least_one_diff = false;
            var new_values = merged_directives[DEFAULT_SRC].filter(function(value) {
              var result = (csp[directive].indexOf(value) != -1);
              at_least_one_diff = at_least_one_diff || result;
              return result;
            });
            if (at_least_one_diff) { // more restrictive than default-src
              merged_directives[directive] = new_values;
            }
            // else default-src is the most restrictive already,
            // -> fall back to default-src the next time again
          }
          // else csp[directive] is "*" and 
          // merged_directives[DEFAULT_SRC] is not "*" but exists
          // -> it must be more restrictive than csp[directive] already
        } else {
          // doesn't exist and no default-src to fall back to
          // -> everything allowed -> current more restrictive or *
          merged_directives[directive] = csp[directive];
        }
      }
    });
  });
  return merged_directives;
}

// Checks if the complement of modified CSP and original CSP is whitelisted,
// i.e. accepted by the server.
// return - a violation report (Object) in case of not whitelisted sources,
//          null otherwise
function matchAgainstWhitelist(merged_original_csp, merged_mod_csp, whitelist) {
  var DEFAULT_SRC = "default-src";
  // "more permissive part" = "merged_mod_csp - merged_original_csp"
  var csp_complement = {};
  CSP_DIRECTIVES.forEach(function(directive) {
    // it's only interesting if it is in at least one of the CSPs.
    // Otherwise it's just handled by the default-src
    // Note, that []!='none' but means "no complementiing elements"
    if ((directive in merged_mod_csp) || (directive in merged_original_csp)) {
      // complement: "a - b"
      var a = (directive in merged_mod_csp) ? merged_mod_csp[directive]
                                            : merged_mod_csp[DEFAULT_SRC];
      if (a === undefined) { a = ["*"]; }
      var b = (directive in merged_original_csp) ? merged_original_csp[directive]
                                                 : merged_original_csp[DEFAULT_SRC];
      if (b === undefined) { b = ["*"]; }

      if (a.indexOf("*") >= 0 && b.indexOf("*") >= 0) { return; } // *-*=[]
      if (a.indexOf("*") < 0 && b.indexOf("*") >= 0) { return; }  // a-*=[]
      if (a.indexOf("*") >= 0 && b.indexOf("*") < 0) {            // *-b~*
        csp_complement[directive] = ["*"];
        return;
      } 
      csp_complement[directive] = a.filter(function(i) {          // a-b=c
        return b.indexOf(i) < 0;
      });
    }
  });
  //console.log("Complement: " + JSON.stringify(csp_complement));

  // Create violation report
  var violation_report = null;
  Object.keys(csp_complement).forEach(function(directive) {
    var violations = (csp_complement[directive].indexOf("*") >= 0)
                      ? ["*"]
                      : csp_complement[directive].filter(function(source) {
                        // not (it is in "general" or the directive)
                          return !((("general" in whitelist)
                                      && (whitelist["general"].indexOf(source) >= 0)
                                   || ((directive in whitelist) 
                                      && whitelist[directive].indexOf(source) >= 0)));
                        });
    if (violations.length > 0) {
      if (violation_report == null) { violation_report = {}; }
      violation_report[directive] = violations;
    }
  });

  return violation_report;
}

function readWhitelist(file_path, callback) {
  fs.readFile(file_path, 'utf8', function (err,data) {
    if (err) { return console.log(err); }
    
    whitelist = JSON.parse(data);
    console.log("Whitelist read from '" + file_path + "'");
    console.log("Applied whitelist:\n" + data);
    callback;
  });
}
