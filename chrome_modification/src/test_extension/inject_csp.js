replaceCSP();

function replaceCSP() {
  chrome.webRequest.onHeadersReceived.addListener(function(details) {
      if (details && details.responseHeaders) {
        details.responseHeaders.forEach(function(header) {
          if (header.name.match(/content-security-policy|^x-.*-csp(-report-only)?$/i)) {
            header.value = "default-src 'none'";

            /*
            for (var p in details.responseHeaders)
              console.log("header info: " + JSON.stringify(details.responseHeaders[p]));
            console.log("inject-csp.js: CSP modified!");
            //*/
          }
        });
        return {responseHeaders: details.responseHeaders};
      }
    },
    {"urls": ["<all_urls>"]},
    ["blocking", "responseHeaders"]
  );
}



// The following code was borrowed (and slightly modified)
// from ./streak-for-gmail/background.js
function extendCSP() {
  // Add ourselves to any Content Security Policy whitelist headers.
  
//  var hosts = server+" ";
  // google jsapi and sharing widgets
  var hosts = "https://*.google.com ";
  hosts += "https://*.linkedin.com ";
  var iframeHosts = "https://*.facebook.com https://*.twitter.com https://twitter.com ";

  chrome.webRequest.onHeadersReceived.addListener(function(details) {
    var replaceHeaders = false;
    details.responseHeaders.forEach(function(header) {
      if (header.name.match(/content-security-policy|^x-.*-csp(-report-only)?$/i)) {
        replaceHeaders = true;

        console.log('found a CSP header, modifying it', details, header);

        ['default-src'].forEach(function(section) {
          header.value = header.value.replace(section+" ", section+" 'unsafe-inline' 'unsafe-eval' "+hosts+iframeHosts);
        });
        ['style-src'].forEach(function(section) {
          header.value = header.value.replace(section+" ", section+" 'unsafe-inline' 'unsafe-eval' "+hosts);
        });
        ['script-src'].forEach(function(section) {
          header.value = header.value.replace(section+" ", section+" 'unsafe-eval' "+hosts);
        });
        ['frame-src','child-src'].forEach(function(section) {
          header.value = header.value.replace(section+" ", section+" "+hosts+iframeHosts);
        });
        ['connect-src','img-src','media-src','font-src'].forEach(function(section) {
          header.value = header.value.replace(section+" ", section+" "+hosts);
        });
      }
    });
    if (replaceHeaders) {
      return {responseHeaders: details.responseHeaders};
    }
  }, {"urls": ["<all_urls>"]}, ["responseHeaders", "blocking"]); 
}

