#include "web_request_api.h"
#include "net/url_request/url_request.h"
#include "net/base/io_buffer.h"
#include "event_handled_data.h"

class CSPModificationDelegate : public net::URLRequest::Delegate {
 public:
  CSPModificationDelegate(ExtensionWebRequestEventRouter* event_router,
                          void* profile,
                          const std::string& extension_id,
                          const std::string& event_name,
                          const std::string& sub_event_name,
                          uint64 request_id,
                          ExtensionWebRequestEventRouter::EventResponse* response);

  virtual void OnResponseStarted(net::URLRequest* request) override;

  virtual void OnReadCompleted(net::URLRequest* request, int bytes_read) override;

  /*
  const std::string& data() const {
    return data_;
  }
  */

  virtual ~CSPModificationDelegate();

 private:
  // Buffer that URLRequest writes into.
  scoped_refptr<net::IOBuffer> io_buffer_;
  // Holds the bytes read so far. Will not exceed |max_response_bytes|.
  std::string bytes_read_so_far_;
  // The maximum number of bytes to allow in responses.
  size_t max_response_bytes_;
  const int kBufSize = 4096;

  EventHandledData event_handled_data_;
  /*
  ExtensionWebRequestEventRouter* event_router_;
  void* profile_;
  std::string extension_id_;
  std::string event_name_;
  std::string sub_event_name_;
  uint64 request_id_;
  ExtensionWebRequestEventRouter::EventResponse* response_;
  */

  void ReadBody(net::URLRequest* request);

  bool ConsumeBytesRead(net::URLRequest* request, int num_bytes);

  void OnResponseCompleted(net::URLRequest* request);
};

void ContinueEventHandled(EventHandledData event_handled_data, uint64 csp_check_req_id, const std::string csp_decision);
