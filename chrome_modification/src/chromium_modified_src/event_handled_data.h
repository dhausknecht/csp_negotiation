#include "web_request_api.h"

struct EventHandledData {
  ExtensionWebRequestEventRouter* event_router;
  void* profile;
  std::string extension_id;
  std::string event_name;
  std::string sub_event_name;
  uint64 request_id;
  ExtensionWebRequestEventRouter::EventResponse* response;

  EventHandledData();
};

