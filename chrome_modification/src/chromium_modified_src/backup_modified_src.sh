#!/bin/bash

CHROME_WEB_REQUEST_DIR=/home/cookie/chromium/src/extensions/browser/api/web_request

echo "--- INFO ---"
echo "current source folder: "$CHROME_WEB_REQUEST_DIR
echo "------------"
for src_file in *.h *.cc 
do
  echo -n "backing up $src_file..."
  cp $CHROME_WEB_REQUEST_DIR/$src_file $src_file
  echo " done"
done
