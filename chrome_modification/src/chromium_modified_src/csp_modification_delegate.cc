#include "csp_modification_delegate.h"
#include "base/callback.h"

CSPModificationDelegate::CSPModificationDelegate(ExtensionWebRequestEventRouter* event_router,
                        void* profile,
                        const std::string& extension_id,
                        const std::string& event_name,
                        const std::string& sub_event_name,
                        uint64 request_id,
                        ExtensionWebRequestEventRouter::EventResponse* response) {
  event_handled_data_.event_router = event_router;
  event_handled_data_.profile = profile;
  event_handled_data_.extension_id = std::string(extension_id);
  event_handled_data_.event_name = std::string(event_name);
  event_handled_data_.sub_event_name = std::string(sub_event_name);
  event_handled_data_.request_id = request_id;
  event_handled_data_.response = response;

  io_buffer_ = scoped_refptr<net::IOBuffer>(new net::IOBuffer(kBufSize));
  max_response_bytes_ = 10000;
}

void CSPModificationDelegate::OnResponseStarted(net::URLRequest* request) {
  if (request->GetResponseCode() == 200) {
    ReadBody(request);
    if (request->status().is_io_pending()) {
      return;
    }
    return;
  }
  printf("OMG, something went wrong! I better cancel it...\n");
  request->Cancel();
  OnResponseCompleted(request);
}

void CSPModificationDelegate::OnReadCompleted(net::URLRequest* request, int bytes_read) {
  ConsumeBytesRead(request, bytes_read);
  //if (!request->status().is_io_pending()) {
  //  base::MessageLoop::current()->Quit();
  //}
  
  OnResponseCompleted(request);
}

CSPModificationDelegate::~CSPModificationDelegate() { /* TODO */ }

// lots of copy paste from "net/proxy/proxy_script_fetcher_impl.cc"
void CSPModificationDelegate::ReadBody(net::URLRequest* request) {
  // Read as many bytes as are available synchronously.
  while (true) {
    int num_bytes = 0;
    if (!request->Read(io_buffer_.get(), kBufSize, &num_bytes)) {
      // Check whether the read failed synchronously.
      if (!request->status().is_io_pending()) {
        OnResponseCompleted(request);
      }
      return;
    }
    if (!ConsumeBytesRead(request, num_bytes)) {
      return;
    }
  } // while (true)
}

bool CSPModificationDelegate::ConsumeBytesRead(net::URLRequest* request, int num_bytes) {
  if (num_bytes <= 0) {
    // Error while reading, or EOF.
    OnResponseCompleted(request);
    return false;
  }
  // Enforce maximum size bound.
  if (num_bytes + bytes_read_so_far_.size() > 
      static_cast<size_t>(max_response_bytes_)) {
    //result_code_ = ERR_FILE_TOO_BIG;
    request->Cancel();
    return false;
  }

  bytes_read_so_far_.append(io_buffer_->data(), num_bytes);
  return true;
}

void CSPModificationDelegate::OnResponseCompleted(net::URLRequest* request) {
  base::Callback<void()> callback = base::Bind(&ContinueEventHandled, 
      event_handled_data_, request->identifier(), bytes_read_so_far_);
//      request->identifier(), event_router_, profile_, extension_id_, event_name_, request_id_, response_);
  callback.Run();
}

void ContinueEventHandled(EventHandledData event_handled_data, uint64 csp_check_req_id, const std::string csp_decision) {
  ExtensionWebRequestEventRouter* event_router_ptr = event_handled_data.event_router;
  std::map<uint64, net::URLRequest*>::iterator it = 
      event_router_ptr->csp_check_requests_.find(csp_check_req_id);
  if (it != event_router_ptr->csp_check_requests_.end()) {
    delete it->second;
    event_router_ptr->csp_check_requests_.erase(csp_check_req_id);
  }
  event_handled_data.event_router->OnCSPUpdateConfirmation(event_handled_data.profile,
                                                           event_handled_data.extension_id,
                                                           event_handled_data.event_name,
                                                           event_handled_data.request_id,
                                                           event_handled_data.response,
                                                           csp_decision);
}

