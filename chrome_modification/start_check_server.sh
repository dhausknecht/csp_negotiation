#!/bin/bash

SERVERFILE=""
POLICYFILE=""

function init {
  echo "Starting CSP check server... ($PWD)"
  SERVERFILE=$1
  POLICYFILE=$2
  nodejs $SERVERFILE --policy=$POLICYFILE
  restart
}

function restart {
  echo "Type new policy 'full/path/to/file' or 'R' for restarting with the same policy:"
  read NEW_POLICY
  if [ "$NEW_POLICY" != "R" ]
    then POLICYFILE=$NEW_POLICY
  fi
  nodejs $SERVERFILE --policy=$POLICYFILE
  restart
}

init $1 $2
