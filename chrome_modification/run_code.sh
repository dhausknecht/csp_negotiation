#!/bin/bash

# path/to/modified/chrome
CHROME_SRC_DIR=/home/cookie/chromium/src/
CHROME_BIN_PATH=out/Debug/

if [ -z "$1" ]
then echo "Argument 'full/path/to/policy.json' (not relative) missing!"
       exit 1
fi

# check for java, mitmdump and nodejs
type mitmdump >/dev/null 2>&1 || { echo >&2 "Please install 'mitmproxy!'"; exit 1; }
type nodejs >/dev/null 2>&1 || { echo >&2 "Please install 'nodejs'!"; exit 1; }

# start CSP report server
#echo -n "Starting CSP report server... "
#gnome-terminal --title "CSP Report Server" -x bash -c "nodejs $PWD/src/server/report_server.js; read"
#echo "done!"

# start CSP check server
echo -n "Starting CSP check server... "
gnome-terminal --title "CSP Check Server" -x bash -c "./start_check_server.sh $PWD/src/server/check_server.js $1"
echo "done!"

# start proxy
echo -n "Starting proxy... "
gnome-terminal --title "CSP Modification Proxy" -x bash -c "echo "Proxy\ started"; mitmdump -q --anticache -s $PWD/src/proxy/mod_report_uri.py; read"
echo "done!"

echo "Starting chrome... "
"$CHROME_SRC_DIR$CHROME_BIN_PATH"chrome --proxy-server=localhost:8080 --allow-running-insecure-content
