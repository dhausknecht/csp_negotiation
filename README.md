# Content Security Policy Endorsement for Browser Extensions #

For a detailed documentation and results please read the respective research paper: [https://danielhausknecht.eu/data/papers/csp_endorsement_dimva15.pdf](https://danielhausknecht.eu/data/papers/csp_endorsement_dimva15.pdf)


### Content ###
The repository contains the following code:

* the modifications of Firefox and Chrome to implement the CSP endorsement mechanism
* the Chrome extensions crawler
* the (modified) extension Rapportive used for the case study