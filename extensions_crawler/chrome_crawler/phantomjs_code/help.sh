#!/bin/bash

while read -r line; do
  gedit $line &
done < <(grep -l "content_security_policy" -R .)
