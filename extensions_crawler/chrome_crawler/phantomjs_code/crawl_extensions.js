var webpage = require('webpage');
var fs = require('fs');
var system = require('system');

var FILE_PATH = "./extension_ids.txt";
var WEBSTORE_URL = 'https://chrome.google.com/webstore/category/extensions?hl=en-GB';
var NUM_LOADS = 10;
var DELAY = 5000;

// Set the command line inputs (if any)
// one input: number of page reloads
// two inputs: number of page reloads and delay between loads to not piss off Google
if (system.args.length === 2) { NUM_LOADS = system.args[1]; }
if (system.args.length === 3) { NUM_LOADS = system.args[1]; DELAY = system.args[2]; }

// the set of all so far seen extensions
var extensions_set = {};

// some init stuff...
var page = webpage.create();
//page.settings.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'
page.settings.userAgent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:32.0) Gecko/20100101 Firefox/32.0";


function getExtensionIds(limit) {
  page.open(WEBSTORE_URL, function (status) {
    window.setTimeout(function() {
      //page.render("screenshot_"+limit+".jpeg");
      // output = "array with extension id-name pairs
      var output = page.evaluate(function() {
        // extensions are displayed in "webstore-S-Bc" elements
        var links = document.getElementsByClassName("webstore-S-Bc");
        var result = new Array();
        for (var i = 0; i < links.length; i++) {
          var href = links.item(i).href;
          // get the ID out of the link and 
          var match = href.match("https://chrome\.google\.com/webstore/detail/(.*)/([a-z]+)\?.*");
          if (match) {
            result.push([match[2], match[1]]); // result[<extension-id>] = name
          }
        }
        // evaluate doesn't allow to return arrays, but strings!
        return JSON.stringify(result);
      });
      output = JSON.parse(output); // back to array!
      // extend the array value to be an object with the download link property
      // which is null for now. But we fix that with populateDownloadLinks later.
      output.forEach(function(elem) {
        if (!(elem in extensions_set)) { extensions_set[elem[0]]= { "name": elem[1], "download": null}; }
      });
      console.log("limit: " + limit + ", extensions: " + Object.keys(extensions_set).length);
      if (limit > 1) {
        getExtensionIds(limit-1);
        
      } else {
        populateDownloadLinks();
      }
    }, DELAY);
  });
}

// prepare for getting the individual download links
function populateDownloadLinks() {
  console.log("Getting download links...");
  // this defines a function for a crazy hack that uses the header information 
  // to get the actual download link as we want it. Otherwise you are redirected
  // and we don't want that... ;-)
  page.onResourceReceived = function(response) {
    for (var h in response.headers) {
      if (response.headers[h].name == "Location") {
        var match = response.url.match("https://clients2.google.com/service/update2/crx\\?response=redirect&prodversion=38.0&x=id%3D([a-z]+)%26installsource%3Dondemand%26uc");
        extensions_set[match[1]].download = response.headers[h].value;
        //console.log(extensions_set[JSON.stringify(match[1]]));
      } 
    }
  }

  // get the actual download links!
  getDownloadLink(Object.keys(extensions_set), 0);
}

// request the google servers to give us the extension download links
// Note that without the hack in populateDownloadLinks, we are redirected to 
// something else (cause Google doesn't really want us to do what we do)
function getDownloadLink(ext_ids, index) {
  page.open("https://clients2.google.com/service/update2/crx?response=redirect&prodversion=38.0&x=id%3D" + ext_ids[index] + "%26installsource%3Dondemand%26uc", function(status) {
    if (index < ext_ids.length) {
      window.setTimeout(function() {
        getDownloadLink(ext_ids, index + 1);
      }, DELAY);
    } else {
      writeExtensionIdsToFile();
    }
  });
}

// write everything to file
function writeExtensionIdsToFile() {
  var ext_ids = "";
  Object.keys(extensions_set).forEach(function(elem) {
    ext_ids += elem + "," + extensions_set[elem].name + "," + extensions_set[elem].download + '\n';
  });
  fs.write(FILE_PATH, ext_ids, "w");

  phantom.exit();
}

/* TEST: START
extensions_set["cahedbegdkagmcjfolhdlechbkeaieki"] = {"name": "FOOBAR", "download": null};
populateDownloadLinks();
// TEST: END */
getExtensionIds(NUM_LOADS);
