#!/bin/bash

###############################################################################
# README:                                                                     #
#-----------------------------------------------------------------------------#
# Expected file format:                                                       #
# each line contains a tripe for a single extension                           #
#   ID,name,download-link                                                     #
# The current script below doesn't allow whitespaces around the commas!       #
#-----------------------------------------------------------------------------#
# Author: Daniel Hausknecht                                                   #
#-----------------------------------------------------------------------------#
# "THE BEER-WARE LICENSE" (Revision 42):                                      #
# As long as you retain this notice you can do whatever you want with this    #
# stuff. If we meet some day, and you think this stuff is worth it, you can   # 
# buy me a beer in return.                                                    #
###############################################################################


EXTENSIONS_ID_FILE=$1
USER_AGENT="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:32.0) Gecko/20100101 Firefox/32.0"
#BASE_URL="https://clients2.google.com/service/update2/crx?response=redirect&prodversion=38.0&x=id%3D"
DELAY=2
EXTENSIONS_DIR="extensions"

if [ ! -d "$EXTENSIONS_DIR" ]; then
  mkdir $EXTENSIONS_DIR
fi
cd $EXTENSIONS_DIR

while read LINE; do
  LINE_ARRAY=(${LINE//,/ })
  url="${LINE_ARRAY[2]}"
  filename=$(basename "$url")
  wget --user-agent "$USER_AGENT" "$url"
  newFilename="${LINE_ARRAY[1]}_$filename"
  mkdir "${LINE_ARRAY[1]}"
  mv "$filename" "${LINE_ARRAY[1]}/$newFilename"
  cd "${LINE_ARRAY[1]}"
  unzip -q "$newFilename"
  cd ..
  sleep $DELAY
done < "../$EXTENSIONS_ID_FILE"

