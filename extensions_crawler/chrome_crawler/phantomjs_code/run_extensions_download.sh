#!/bin/bash

# get extension IDs and store them in a file (+ meta data like name)
phantomjs crawl_extensions.js 20 3000

# access the file and actually download the extensions
./download_extensions.sh extension_ids.txt
