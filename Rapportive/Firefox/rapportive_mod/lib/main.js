var data = require("self").data;
var pageMod = require("page-mod");
var { MatchPattern } = require("match-pattern");
var tabs = require("tabs");
var {Cc, Ci, Cr} = require("chrome");

var gmailPattern = /https?:\/\/mail\.google\.com\/(mail|a\/).*/;
var gmailMatcher = new MatchPattern(gmailPattern);


exports.main = function (options, callbacks) {
    var contentScript = data.url("user.js");

    console.log("Loading Rapportive addon (" + options.loadReason + ")");

    // Whatever reason we were loaded, always register the Gmail content script
    pageMod.PageMod({
        include: gmailPattern,
        contentScriptFile: contentScript
    });

    switch (options.loadReason) {
    case "install":
    case "enable":
        rapportiviseExistingGmails(contentScript);
        break;
    case "startup":
    case "upgrade":
    case "downgrade":
        // no-op
        break;
    default:
        console.log("Unexpected loadReason: " + options.loadReason);
    }
};


exports.onUnload = function (reason) {
    exports.headerReceivedObserver.unregister();
    console.log("Unloading Rapportive addon (" + reason + ")");
};


function rapportiviseExistingGmails(contentScript) {
    console.log("Looking for Gmail tabs...");

    for each (var tab in tabs) {
        console.log("Testing tab: " + tab.url);

        if (gmailMatcher.test(tab.url)) {
            console.log("Tab " + tab.title + " is some form of Gmail!");

            tab.attach({
                contentScriptFile: contentScript
            });
        }
    }
}

var myVisitor = {
  visitHeader: function(aHeader, aValue) {
    if (aHeader.match(/content-security-policy|^x-.*-csp(-report-only)?$/i)) {
      console.log("\n"+aHeader+"\n");
    }
  }
};

// make Rapportive work with CSP:
exports.headerReceivedObserver = {
  isRegistered: false,

  observe: function(subject, topic, data) {
    if (topic == "http-on-examine-response") {
      var httpChannel = subject.QueryInterface(Ci.nsIHttpChannel);
      var host = "";
      try {
        host = httpChannel.getRequestHeader("Host");
      } catch (e) {
        console.log("Couldn't even get the host... " + e.message);
        return;
      }
      if ("mail.google.com" == host) {
        try {
          var csp = httpChannel.getResponseHeader("content-security-policy");

          ['script-src'].forEach(function(section) {
            csp = csp.replace(section+" ", section+" https://rapportive.com/ https://platform.linkedin.com/ https://www.linkedin.com/ ");
          });
          ['frame-src'].forEach(function(section) {
            csp = csp.replace(section+" ", section+" https://api.linkedin.com/ ");
          });

          httpChannel.setResponseHeader("content-security-policy", csp, false);
        } catch (e) {
        }
      }
    } else {
      console.log("How is that even possible? :-O");
    }
  },

  observerService() {
    return Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
  },

  register: function() {
    if(this.isRegistered) { console.log("JESUS FUCKING CHRIST!!!"); return; }
    this.observerService().addObserver(this, "http-on-examine-response", false);
    this.isRegistered = true;
  },

  unregister: function() {
    if(!this.isRegistered) { console.log("JESUS FUCKING CHRIST!!!"); return; }
    this.observerService().removeObserver(this, "http-on-examine-response");
    this.isRegistered = false;
  }
};

exports.headerReceivedObserver.register();
